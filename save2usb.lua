--[[ HOW to USE
# Compile this and save byte-code to internal memory.
make directory /lua
lua -o /lua/save2usb.luao usb1:/save2usb.lua

lua use on                                      #DEFAULT on#
schedule at N startup * lua /lua/save2usb.luao  #N is any schedule ID#
--]]
EXTERNAL_MEM = 'USB1:/config.txt'
INTERNAL_MEM = '0'
PATTERN = string.regexp('Configuration saved in ("CONFIG[0-4]"|"(usb|sd)1:)')
PATTERN_EXMEM = string.regexp('Configuration saved in "(usb|sd)1:')
while true do
  rtn, str = rt.syslogwatch(PATTERN)
  if rtn > 0 and not string.find(str[1], 'by LUA', -#'by LUA', true) then
    rt.command($'save ${string.find(str[1], PATTERN_EXMEM) and INTERNAL_MEM or EXTERNAL_MEM}')
  end
end
